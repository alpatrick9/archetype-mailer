<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/6/18
 * Time: 5:22 PM
 */

namespace AppBundle\Services;


use JMS\DiExtraBundle\Annotation\Service;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\InjectParams;
use PHPMailer\PHPMailer\PHPMailer;
use SecIT\ImapBundle\Service\Imap;

/**
 * Class MailerServices
 * @package AppBundle\Services
 * @Service("services.mailer", public=true)
 */
class MailerServices
{
    /**
     * @var $imap Imap
     */
    private $imap;

    /**
     * MailerServices constructor.
     * @InjectParams({
     *  "imap" = @Inject("secit.imap")
     * })
     */
    public function __construct(Imap $imap)
    {
        $this->imap = $imap;
    }


    public function isConnect() {
        return $this->imap->testConnection("default_connection");
    }

    public function sendEmail($to, $subject, $content, $pj = null) {
        $mail = new PHPMailer(false);                              // Passing `true` enables exceptions
        try {
            //Server settings
            //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
            $mail->isSMTP();
            $mail->Host = "smtp.gmail.com";
            $mail->SMTPAuth = true;
            $mail->Username = "patrick@threeshells.ovh";
            $mail->Password = "RajAlPatrick9";
            $mail->SMTPSecure = "ssl";
            $mail->Port = 465;

            //Recipients
            $mail->setFrom("patrick@threeshells.ovh", 'Test Patrick');
            $mail->addAddress($to);

            //Attachments
            //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            /*if($pj) {
                $target = $this->contenaire->getParameter('pj_dir') . "/";
                foreach ($pj as $item) {
                    $mail->addAttachment($target . $item);
                }
            }*/

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body    = $content;
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();
        } catch (\Exception $e) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }
    }
}