<?php

namespace AppBundle\Controller;

use AppBundle\Services\MailerServices;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use JMS\DiExtraBundle\Annotation\Inject;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{

    /**
     * @var $mailerServices MailerServices
     * @Inject("services.mailer")
     */
    private $mailerServices;
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        ///var_dump($this->mailerServices->isConnect());
        $this->mailerServices->sendEmail("alain.patrick.raj@gmail.com", "test","<h1>Test</h1>");
        return new Response("index");
    }
}
